package com.example.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private TextView tvmainOne;
private String url = "http://192.168.29.230:3000/news";
OkHttpClient okHttpClient = new OkHttpClient();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Call call = okHttpClient.newCall(request);


                  try {
                    Response response = call.execute() ;
                    String result = response.body().string();
                    Log.i("TAG", result);


                  }catch (Exception e){
                    e.printStackTrace();
                  }

                call.enqueue(new Callback(){
                    public void onFailure(Call call, IOException e){

                    }
                    public void onResponse(Call call,Response response)throws  IOException{
                    String result = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvmainOne.setText(result);
                        }
                    });
                    }

                });
            }
        }).start();
    }

    private void initView() {
        tvmainOne = (TextView) findViewById(R.id.tvmain_one);
    }
}